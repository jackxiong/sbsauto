import requests
import unittest
from pprint import pprint
import json
import logging

logging.basicConfig(level=logging.INFO)


class RadioDataAPI:
    base_url = "https://www.sbs.com.au/guide/ajax_radio_program_catchup_data/"

    @classmethod
    def get_mp3_list(cls, language="mandarin", location="NSW", city="Sydney"):
        url = cls.base_url + f"language/{language}/location/{location}/sublocation/{city}"
        logging.info(f"GET URL => {url}")
        res = requests.get(url, headers=None, params=None, timeout=10, verify=True)
        if not res.ok:
            cls.raise_http_error(url, res.status_code, res.text)

        a_dict = res.json()
        logging.info("GET RESPONSE => %s" % json.dumps(a_dict, indent=4))
        return a_dict


class TestRadioDataAPI(unittest.TestCase):
    def test_get_mandarin_sydney_data(self):
        mp3_list = RadioDataAPI.get_mp3_list()
        self.assertEqual(len(mp3_list), 8)
        self.validate_each(mp3_list)

    def validate_each(self, items):
        for item in items:
            self.assertIsNotNone(item["label"])
            self.assertEqual(item["program"], "Mandarin")
            self.assertEqual(item["channelName"], "SBSRadio1")
            self.assertGreater(item["endTime"], item["startTime"])
            self.assertEqual(item["onDigitalRadio"], True)
            self.assertEqual(item["analogueFrequency"], "1107am")
            self.assertIn("ONDemand_SBS_RADIO1_07_00.mp3", item["archiveAudio"]["mp3"])
            self.assertIn("ONDemand_SBS_RADIO1_07_00.mp3", item["archiveAudio"]["m4a"])
            self.assertIn("ONDemand_SBS_RADIO1_07_00.flv", item["archiveAudio"]["flv"])
