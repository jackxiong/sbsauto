var reporter = require('cucumber-html-reporter');

var options = {

        theme: 'bootstrap',

        jsonFile: 'reports/report.json',

        output: 'reports/cucumber_report.html',

        reportSuiteAsScenarios: true,

        scenarioTimestamp: true,

        launchReport: true,

        metadata: {

            "Title": "SBS Sample Report",

            "Browser": "Chrome",

            "Platform": "Mac 10.15.6 (19G2021)"

        }

    };


reporter.generate(options);